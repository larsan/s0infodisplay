(function a () {
    var lastMinute = null;
    setInterval(function () {
        var d = new Date();
        var minute = d.getMinutes()
        if (minute != lastMinute) {
            lastMinute = minute;
            var hour = d.getHours();
            var found = $('#bus .time').each(function () {
                var depart = $(this).children('span:first').text().split(':');
                depart = depart.map(function (a) {return parseInt(a);});
                if (depart[0] < hour) {
                    depart[0] += 24;
                }
                var diff = (depart[0]-hour)*60 + depart[1]-minute - 1;
                var text = diff < 0 ? 'now' : diff+' min'
                var color = diff <= 5 ? '#dc322f' : '';
                $(this).find('.until').text(text).attr('color', color);
            }).length;
            if (found === 0) {
                lastMinute = null;
            }
        }
    }, 1000);
    $('#bus').on('content', function (ev, name) {
        lastMinute = null;
    });
})();
