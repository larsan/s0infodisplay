import fs from 'fs';
import httpreq from 'httpreq';
import Mustache from 'mustache';

const TEMPLATES = {
  main: fs.readFileSync('modules/diagrams/main.mustache', 'utf-8'),
  data: fs.readFileSync('modules/diagrams/data.mustache', 'utf-8'),
};

function fetchData(id, cb) {
  const url = `http://shiny.tinyhost.de/php/getdata.php?time=1&id[]=${id}`;
  httpreq.get(url, { binary: true }, (err, res) => {
    try {
      cb(res.body.toString().split('\n'));
    } catch (e) {
      // Swallow Error
    }
  });
}

const DATA = { power: '[]', devices: '[]' };

function handleData(prop, data) {
  let line = [];
  const out = [];
  for (let i = 1; i < data.length - 1; i++) {
    line = data[i].split(',');
    if (line.length === 2) {
      out.push([(new Date(line[0])).getTime(), parseFloat(line[1])]);
    }
  }
  DATA[prop] = JSON.stringify(out);
}

function update(cb) {
  let counter = 0;
  const next = function() {
    counter += 1;
    if (counter === 2) {
      cb();
    }
  };
  fetchData(1, (data) => {
    handleData('power', data);
    next();
  }); //power
  fetchData(4, (data) => {
    handleData('devices', data);
    next();
  }); //devices
}

module.exports = function(io) {
  setInterval(() => {
    update(() => {
      io.emit('diagrams.data', Mustache.render(TEMPLATES.data, DATA));
    });
  }, 5000);
  io.on('connect', (sock) => {
    sock.emit('diagrams', Mustache.render(TEMPLATES.main, {}));
  });
};
