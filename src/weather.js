import fs from 'fs';
import httpreq from 'httpreq';
import Mustache from 'mustache';

///CONFIG
const APPID = 'fdc3690e6f9a7572128fe4012b4a2500';
const CITYID = '2945024';

/// STATICS
const directions = { NNE: 11.25, NE: 33.75, ENE: 56.25, E: 78.75, ESE: 101.25, SE: 123.75, SSE: 146.25, S: 168.75, SSW: 191.25, SW: 213.75, WSW: 236.25, W: 258.75, WNW: 281.25, NW: 303.75, NNW: 326.25, N: 348.75 };
const iconBaseURL = 'http://openweathermap.org/img/w/';
const TEMPLATE = fs.readFileSync('modules/weather/template.mustache', 'utf-8');

function fetchCurrent(cityid, cb) {
  const url = `http://api.openweathermap.org/data/2.5/weather?units=metric&id=${cityid}&appid=${APPID}`;
  httpreq.get(url, (err, res) => {
    const dat = JSON.parse(res.body);
    if (dat.cod) {
      cb({
        temp: dat.main.temp,
        wind: { speed: dat.wind.speed, dir: degToDirection(dat.wind.deg) },
        pressure: dat.main.pressure,
        humidity: dat.main.humidity,
        main: dat.weather[0].main,
        desc: dat.weather[0].description,
        icon: `${iconBaseURL + dat.weather[0].icon }.png`,
      });
    }
  });
}

function fetchForecast(cityid, count, cb) {
  const url = `http://api.openweathermap.org/data/2.5/forecast?units=metric&id=${cityid}&appid=${APPID}`;
  httpreq.get(url, (err, res) => {
    const raw = JSON.parse(res.body);
    if (raw.list) {
      const dat = raw.list.slice(0, count);
      cb(dat.map((d) => {
        const date = new Date(d.dt * 1000);
        return {
          time: `${pad(date.getHours(), 2)}:${pad(date.getMinutes(), 2)}`,
          temp: d.main.temp,
          wind: { speed: d.wind.speed, dir: degToDirection(d.wind.deg) },
          main: d.weather[0].main,
          desc: d.weather[0].description,
          icon: `${iconBaseURL + d.weather[0].icon }.png`,
        };
      }));
    }
  });
}

function degToDirection(deg) {
  let dir = 'N';
  for (const i in directions) {
    if (deg > directions[i]) {
      dir = i;
    }
  }
  return dir;
}

module.exports = function(io) {
  const context = {};
  let firstTime = true;
  const update = function(firstUpdateCb) {
    const then = function() {
      if (context.current && context.forecast && firstTime) {
        firstUpdateCb();
        firstTime = false;
      }
    };
    fetchCurrent(CITYID, (current) => {
      context.current = current;
      then();
    });
    fetchForecast(CITYID, 6, (forecast) => {
      context.forecast = forecast;
      then();
    });
  };
  update(() => {
    const pushToClients = function(sock) {
      sock.emit('weather', Mustache.render(TEMPLATE, context));
    };
    io.on('connect', pushToClients);
    pushToClients(io);
  });
  setInterval(update, 10 * 60 * 1000);
};
