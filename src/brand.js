import fs from 'fs';
import httpreq from 'httpreq';
import mustache from 'mustache';

const URL = 'https://stratum0.org/status/status.json';
const TEMPLATES = {
  template: fs.readFileSync('modules/brand/template.mustache', 'utf-8'),
  status: fs.readFileSync('modules/brand/status.mustache', 'utf-8'),
};

let status = {};

function renderStatus(sock, everything) {
  const sendInner = function() {
    status.random = `${Math.random()}`;
    sock.emit('brand.status', mustache.render(TEMPLATES.status, status));
  };
  if (everything) {
    sock.emit('brand', mustache.render(TEMPLATES.template, {}));
    setTimeout(sendInner, 3000);
  } else {
    sendInner();
  }
}

function fetchStatus(cb) {
  httpreq.get(URL, (err, res) => {
    const state = JSON.parse(res.body).state;
    cb(state);
  });
}

module.exports = function(io) {
  let firstTime = true;
  function update() {
    fetchStatus((state) => {
      if (status.lastchange !== state.lastchange) {
        const d = new Date(state.lastchange * 1000);
        state.since = `${DOW[d.getDay()]}, ${pad(d.getHours(), 2)}:${
        pad(d.getMinutes(), 2)}`;
        status = state;
        renderStatus(io);
      }
      if (firstTime && status) {
        io.on('connection', (sock) => {
          renderStatus(sock, true);
        });
        renderStatus(io, true);
        firstTime = false;
      } else {
        renderStatus(io, false);
      }
    });
  }
  setInterval(update, 2 * 60 * 1000); //every 2 minutes
  update();
};
