function date() {
  const d = new Date();
  return `${DOW[d.getDay()]}, ${pad(d.getDate(), 2)}.${pad(d.getMonth() + 1, 2)}.${d.getFullYear()} ${pad(d.getHours(), 2)}:${pad(d.getMinutes(), 2)}:${pad(d.getSeconds(), 2)}`;
}


module.exports = function(io) {
  setInterval(() => {
    io.emit('date', `<div align="right"><h2>${date()}&nbsp;</h2></div>`);
  }, 1000);
};
