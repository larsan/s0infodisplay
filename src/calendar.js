/* eslint no-mixed-operators: 0 */
import fs from 'fs';
import httpreq from 'httpreq';
import ical from 'ical.js';
import Mustache from 'mustache';
import time from 'time';

const URL = 'https://stratum0.org/kalender/termine.ics';
const TEMPLATE = fs.readFileSync('modules/calendar/template.mustache', 'utf-8');
let CALENDAR;

const TZOFFSET = new time.Date().getTimezoneOffset() * 60;


function getData(count, cb) {
  httpreq.get(URL, (err, res) => {
    const ics = res.body.replace(/[0-9]{8}T[0-9]{6}/g, '$&Z');
    const cal = new ical.Component(ical.parse(ics));
    const rawEvents = cal.getAllSubcomponents('vevent').map((raw) => new ical.Event(raw));
    const events = [];
    const p = function(p) {
      return ev.component.getFirstPropertyValue(p);
    };
    let ev = p;
    for (const i in rawEvents) { // eslint-disable-line
      ev = rawEvents[i];
      if (ev.isRecurring()) {
        const iter = ev.iterator();
        const duration = p('dtend').toUnixTime() - p('dtstart').toUnixTime();
        for (let i = 0; i < 100; i++) {
          const next = iter.next();
          if (next === undefined) {
            break;
          }
          const start = next.toUnixTime() + TZOFFSET;
          events.push({
            title: p('summary'),
            start,
            end: start + duration,
          });
        }
      } else {
        events.push({
          title: p('summary'),
          start: p('dtstart').toUnixTime() + TZOFFSET,
          end: p('dtend').toUnixTime() + TZOFFSET,
        });
      }
    }
    events.sort((a, b) => a.start - b.start);
    const threshold = Date.now() / 1000 - 5 * 60 * 60; //show 5 hours ago
    let i;
    for (i in events) {
      if (events[i].end > threshold) {
        i = parseInt(i, 10);
        break;
      }
    }
    const now = new Date();
    cb(events.slice(i, i + count).map((ev) => {
      const start = new Date(ev.start * 1000);
      if (start.getMonth() === now.getMonth() && start.getDate() === now.getDate()) {
        ev.startRendered = `${pad(start.getHours(), 2)}:${pad(start.getMinutes(), 2)}`;
      } else {
        ev.startRendered = `${DOW[start.getDay()]}, ${pad(start.getDate(), 2)
        }.${pad(start.getMonth() + 1, 2)}. ${pad(start.getHours(), 2)
        }:${pad(start.getMinutes(), 2)}`;
      }
      const end = new Date(ev.end * 1000);
      if (ev.end - ev.start >= 60 * 60 * 24) {
        ev.endRendered = `${DOW[end.getDay()]} ${pad(end.getHours(), 2)
        }:${pad(end.getMinutes(), 2)}`;
      } else {
        ev.endRendered = `${pad(end.getHours(), 2)}:${pad(end.getMinutes(), 2)}`;
      }
      const dur = Math.floor((ev.end - ev.start) / 60);
      ev.duration = `${pad(Math.floor(dur / 60), 2) }:${ pad((dur % 60), 2)}`;
      if (start.getTime() < now.getTime()) {
        if (end.getTime() < now.getTime()) {
          ev.past = true;
        } else {
          ev.now = true;
        }
      }
      return ev;
    }));
  });
}

module.exports = function(io) {
  function update(firstRunCallback) {
    getData(8, (data) => {
      CALENDAR = data;
      io.emit('calendar', Mustache.render(TEMPLATE, CALENDAR));
      if (firstRunCallback) {
        firstRunCallback();
        firstRunCallback = null; // eslint-disable-line
      }
    });
  }
  update(() => {
    const pushToClients = function(sock) {
      sock.emit('calendar', Mustache.render(TEMPLATE, CALENDAR));
    };
    io.on('connect', pushToClients);
    pushToClients(io);
  });
  setInterval(update, 600000);
};
