/* eslint no-console: 0 */
require('babel-polyfill')
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

server.listen(8000);

app.use(express.static(`${__dirname }/../public`));

const instanceClients = [];
io.on('connection', (socket) => {
    socket.on('ident', (client) => {
        if (instanceClients.indexOf(client) === -1) {
            instanceClients.push(client);
            socket.emit('meta', 'reload');
        }
    });
});

// for reasons
global.DOW = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
global.pad = function(n, width, z = '0') {
    // eslint-disable-next-line
    n = String(n);
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

const normalizedPath = require('path').join(__dirname);
require('fs').readdirSync(normalizedPath).forEach((file) => {
    if (file.endsWith('.js') && !file.includes('main.js')) {
        try {
            require(`./${ file}`)(io);
            console.log(`${file} loaded`);
        } catch (e) {
            console.log(`Error loading ${file}`);
            console.log(e);
        }
    }
});
